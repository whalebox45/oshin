import json
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from statsmodels.tsa.arima_model import ARMA
from statsmodels.tsa.statespace.sarimax import SARIMAX
from sklearn.metrics import mean_squared_error
from math import sqrt
from statsmodels.tsa.api import ExponentialSmoothing, SimpleExpSmoothing, Holt
import pickle
import uuid
import sys

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

input_num = int(sys.argv[1])

data_num = []
for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        continue
    if (mv_x % 100) == 0:
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)

    for x in range(len(data)):
        data_num.append(int(data[x][2]))

data_rows = [x for x in range(len(data_num))]

data_rows = np.asarray(data_rows)
data_num = np.asarray(data_num)

training_data, testing_data = data_num[:int(data_num.shape[0] * 0.9)], data_num[int(data_num.shape[0] * 0.9):]
training_data_rows, testing_data_rows = data_rows[:int(data_rows.shape[0] * 0.9)], data_rows[int(data_rows.shape[0] * 0.9):]

dynamic_rows = testing_data_rows.tolist()

for x in range(input_num):
	dynamic_rows.append(testing_data_rows[testing_data_rows.shape[0] - 1] + x)

dynamic_rows = np.asarray(dynamic_rows)

# LinearRegression()

lin_reg_model = LinearRegression()
lin_reg_model.fit(training_data_rows.reshape(training_data_rows.shape[0], 1), training_data.reshape(training_data.shape[0], 1))
lin_reg_training_show = lin_reg_model.predict(training_data_rows.reshape(training_data_rows.shape[0], 1))
lin_reg_testing_show = lin_reg_model.predict(dynamic_rows.reshape(dynamic_rows.shape[0], 1))

# rms = sqrt(mean_squared_error(testing_data, lin_reg_testing_show)) # true # predict
# rms = np.mean(np.abs((testing_data - lin_reg_testing_show) / testing_data)) * 100
# rms = rmse(lin_reg_testing_show, testing_data)
# print("LinearRegression rms: {}".format(rms))

# LinearRegression degree=2()

polynomialFeatures_model = PolynomialFeatures(degree=2)

poly_fited = polynomialFeatures_model.fit_transform(training_data_rows.reshape(training_data_rows.shape[0], 1))

lin_reg_model_3 = LinearRegression()
lin_reg_model_3.fit(poly_fited, training_data.reshape(training_data.shape[0], 1))

lin_reg_training_show_3 = lin_reg_model_3.predict(polynomialFeatures_model.transform(training_data_rows.reshape(training_data_rows.shape[0], 1)))
lin_reg_testing_show_3 = lin_reg_model_3.predict(polynomialFeatures_model.transform(dynamic_rows.reshape(dynamic_rows.shape[0], 1)))

#rms = sqrt(mean_squared_error(testing_data, lin_reg_testing_show_3))
#rms = np.mean(np.abs((testing_data - lin_reg_testing_show_3) / testing_data)) * 100
# rms = rmse(lin_reg_testing_show_3, testing_data)
# print("LinearRegression degree=2 rms: {}".format(rms))

# Traditional method

bias_mins = training_data[training_data.shape[0] - 1] - training_data[0]
average_mins = bias_mins / training_data.shape[0] - 1

training_data_tra = []
testing_data_tra = []

for x in range(data_num.shape[0] + input_num):
    if x >= training_data.shape[0]:
        testing_data_tra.append(training_data[0] + average_mins * x)
    else:
        training_data_tra.append(training_data[0] + average_mins * x)

# rms = sqrt(mean_squared_error(testing_data, testing_data_tra))
# rms = np.mean(np.abs((testing_data - np.asarray(testing_data_tra)) / testing_data)) * 100
# rms = rmse(testing_data_tra, testing_data)
# print("Traditional method rms: {}".format(rms))

# Naive-forecast

result_naive = [training_data[training_data.shape[0] - 1] for x in range(dynamic_rows.shape[0])]
result_naive = np.asarray(result_naive)

# rms = rmse(result_naive, testing_data)
# print("Naive-forecast rms: {}".format(rms))

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], training_data, color='lightblue', label='training data')
plt.plot(data_rows[int(data_rows.shape[0] * 0.9):], testing_data, color='orange', label='testing data')

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], training_data_tra, color='blue', label='tra_method training')
plt.plot(dynamic_rows, testing_data_tra, color='red', label='tra_method testing')
plt.xlabel('Date')
plt.ylabel('Passenger')
plt.title('Line chart')
plt.legend()
uuid_rand = str(uuid.uuid4())
print(uuid_rand)
plt.savefig('public/images/{}.png'.format(uuid_rand))
plt.clf()

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], training_data, color='lightblue', label='training data')
plt.plot(data_rows[int(data_rows.shape[0] * 0.9):], testing_data, color='orange', label='testing data')

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], lin_reg_training_show, color='blue', label='LR training')
plt.plot(dynamic_rows, lin_reg_testing_show, color='red', label='LR testing')
plt.xlabel('Date')
plt.ylabel('Passenger')
plt.title('Line chart')
plt.legend()
uuid_rand = str(uuid.uuid4())
print(uuid_rand)
plt.savefig('public/images/{}.png'.format(uuid_rand))
plt.clf()

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], training_data, color='lightblue', label='training data')
plt.plot(data_rows[int(data_rows.shape[0] * 0.9):], testing_data, color='orange', label='testing data')

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], lin_reg_training_show_3, color='blue', label='LR(2) training')
plt.plot(dynamic_rows, lin_reg_testing_show_3, color='red', label='LR(2) testing')
plt.xlabel('Date')
plt.ylabel('Passenger')
plt.title('Line chart')
plt.legend()
uuid_rand = str(uuid.uuid4())
print(uuid_rand)
plt.savefig('public/images/{}.png'.format(uuid_rand))
plt.clf()

plt.plot(data_rows[:int(data_rows.shape[0] * 0.9)], training_data, color='lightblue', label='training data')
plt.plot(data_rows[int(data_rows.shape[0] * 0.9):], testing_data, color='orange', label='testing data')

plt.plot(dynamic_rows, result_naive, color='red', label='naive-forecast testing')

plt.xlabel('Date')
plt.ylabel('Passenger')
plt.title('Line chart')
plt.legend()
uuid_rand = str(uuid.uuid4())
print(uuid_rand)
plt.savefig('public/images/{}.png'.format(uuid_rand))
plt.clf()
# plt.show()
