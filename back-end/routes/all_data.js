var express = require('express');
var router = express.Router();
var all_data = require('../public/jsons/all-data.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send(all_data);
});

module.exports = router;
