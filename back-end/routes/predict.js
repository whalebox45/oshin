var express = require('express');
var router = express.Router();
var ps = require('python-shell');

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.query.predict_days);

  var input_num = req.query.predict_days;

  var options = {
    mode: 'text',
    pythonOptions: ['-u'],
    scriptPath: './',
    args: [input_num]
  };
  ps.PythonShell.run('ml_linearR.py', options, function (err, results) {
    if (err) throw err;
    // results is an array consisting of messages collected during execution
    //console.log('results: %j', results);
    results.forEach(function (element){
      console.log(element); 
    });
    res.send(JSON.stringify({state: 'ok', URL01: results[0], URL02: results[1], URL03: results[2], URL04: results[3]}));
  });
});

module.exports = router;


 
