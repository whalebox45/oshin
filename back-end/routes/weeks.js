var express = require('express');
var router = express.Router();

var monday = require('../public/jsons/monday.json');
var tuesday = require('../public/jsons/tuesday.json');
var wednesday = require('../public/jsons/wednesday.json');
var thursday = require('../public/jsons/thursday.json');
var friday = require('../public/jsons/friday.json');
var saturday = require('../public/jsons/saturday.json');
var sunday = require('../public/jsons/sunday.json');

/* GET home page. */
router.get('/monday', function(req, res, next) {
    res.send(monday);
});

router.get('/tuesday', function(req, res, next) {
    res.send(tuesday);
});

router.get('/wednesday', function(req, res, next) {
    res.send(wednesday);
});

router.get('/thursday', function(req, res, next) {
    res.send(thursday);
});

router.get('/friday', function(req, res, next) {
    res.send(friday);
});

router.get('/saturday', function(req, res, next) {
    res.send(saturday);
});

router.get('/sunday', function(req, res, next) {
    res.send(sunday);
});


module.exports = router;
