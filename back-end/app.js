var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var all_data_Router = require('./routes/all_data');
var weeks_Router = require('./routes/weeks');
var predict_Router = require('./routes/predict');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/all_data', all_data_Router)
app.use('/weeks', weeks_Router);
app.use('/predict', predict_Router);

module.exports = app;
