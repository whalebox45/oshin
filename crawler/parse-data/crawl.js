var request = require("request");
var cheerio = require("cheerio");
var tableToCsv = require('node-table-to-csv');

var iconv = require('iconv-lite');
var fs = require('fs');


var date_mv = 0;

for (date_mv = 8504; date_mv < 10712; date_mv++) {
    if (date_mv % 100 == 13) {
        date_mv = date_mv + 100 - 12;
        continue;
    } else {
        var body = fs.readFileSync('./passenger_data/' + date_mv.toString() + '.csv', 'utf8');
        console.log(body);
        //var str = iconv.decode(body, "BIG5");
        //csv = tableToCsv(body);
        console.log("CSV===");
        console.log(body);
        var one_day = body.match(/\"[0-9]+\/[0-9]+\/[0-9]+\",\".*\",\".*\"/gm);
        for(a = 0; a < one_day.length; a++) {
            one_day[a] = one_day[a].replace(/["]+/g, '');
        }
        new_arr = [];
        for(a = 0; a < one_day.length; a++) {
            new_arr.push(one_day[a].split(','));
        }
        
        for(a = 0; a < new_arr.length; a++) {
            var res_concat = '';
            for(v = 2; v < new_arr[0].length; v++) {
                res_concat = res_concat.concat(new_arr[a][v]);
            }
            new_arr[a][2] = res_concat;
        }
        
        res_arr = []
        for(a = 0; a < new_arr.length; a++) {
            var temp_arr = [];
            for(v = 0; v < 3; v++) {
                temp_arr.push(new_arr[a][v]);
            }
            res_arr.push(temp_arr);
        }
        console.log(res_arr);
        
        fs.writeFileSync(date_mv.toString() + ".json", JSON.stringify(res_arr), function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }
}