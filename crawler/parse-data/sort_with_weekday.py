# -*- coding: utf-8 -*-
import json
import sys

if sys.version_info < (3, 0):
    sys.stdout.write("Sorry, requires Python 3.x, not Python 2.x\n")
    sys.exit(1)

weekday = [
    ("sunday","日"),
    ("monday","一"),
    ("tuesday","二"),
    ("wednesday","三"),
    ("thursday","四"),
    ("friday","五"),
    ("saturday","六")
]

data_json = []

for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        continue
    if (mv_x % 100) == 0:
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)
        for x in range (len(data)):
            data_json.append(data[x])

for w in weekday:
    print (w[1]) # 日 一 二 三 ...
    weekjson = []
    for x in range(len(data_json)):
        if(data_json[x][1] == w[1]):
            weekjson.append(data_json[x])
    fw = open("./passenger_data_json_wk/"+str(w[0])+".json","w")
    print(weekjson, file=fw)
    fw.close()
