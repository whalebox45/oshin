var Crawler = require("crawler");
 
var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            data = [];
            var i = 0;
            while (1){
                try {
                    var each_data = new Object();
                    each_data.date = $(".xl7516464")[i].children[0].data;
                    each_data.weeks = $(".xl7816464")[i].children[1].data;
                    each_data.population = $(".xl7616464")[i].children[0].data.replace(/[\n\r\s]/g, '');
                    data.push(each_data);
                } catch (e) {
                    break;
                }
                i++;
            }
            console.log(data);
        }
        done();
    }
});
 
// Queue just one URL, with default callback
c.queue('http://web.metro.taipei/RidershipCounts/c/10711.htm');