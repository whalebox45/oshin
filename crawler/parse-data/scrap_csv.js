var request = require("request");
var cheerio = require("cheerio");
var tableToCsv = require('node-table-to-csv');

var iconv = require('iconv-lite');

request({
  uri: "http://web.metro.taipei/RidershipCounts/c/"+process.argv[2]+".htm",encoding:null
}, function(error, response, body) {
    var str = iconv.decode(body, "big5");
    csv = tableToCsv(str);
    console.log(csv);
});
