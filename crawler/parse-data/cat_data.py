import json
import sys

data_json = []

for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        continue
    if (mv_x % 100) == 0:
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)
        for x in range (len(data)):
            data_json.append(data[x])

fw = open("./passenger_data_json/all.json","w")
print(data_json,file=fw)
fw.close()