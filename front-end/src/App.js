
import React, { Component } from 'react';
import logo from './logo.svg';
//import { BarChart } from "react-d3-components";
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Label, Brush } from "recharts";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Navbar, Nav, NavItem, Grid, Row, Col, Panel, FormGroup, FormControl, HelpBlock, ControlLabel, Button } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className>
        <Router>
          <div>
            <Navbar>
              <Navbar.Header>
                <Navbar.Brand >
                  <Link to="/">Metro Info</Link>
                </Navbar.Brand>
              </Navbar.Header>
              <Nav>
                <LinkContainer to="/weeks">
                  <NavItem eventKey={1}>Weeks</NavItem>
                </LinkContainer>
                <LinkContainer to="/predict">
                  <NavItem eventKey={2}>Predict</NavItem>
                </LinkContainer>
                <LinkContainer to="/gallery">
                  <NavItem eventKey={2}>Gallery</NavItem>
                </LinkContainer>
              </Nav>
            </Navbar>
            <Grid>
              <Route path="/" exact component={Index} />
              <Route path="/weeks" component={Weeks} />
              <Route path="/predict" component={Predict} />
              <Route path="/gallery" component={Gallery} />
            </Grid>
          </div>
        </Router>
      </div>
    )
  }
}

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [[[]]],
      data_loads: false
    };
  }

  componentDidMount() {
    fetch('http://localhost:8000/all_data')//change it to WAN IP before we deploy our project
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson,
          data_loads: true
        });
      });
  }

  render() {
    //let data = [
    //  {name: 'day', uv: 4000},
    //  {name: 'day', uv: 3000},
    //  {name: 'day', uv: 2000},
    //  {name: 'day', uv: 2780},
    //  {name: 'day', uv: 1890},
    //  {name: 'day', uv: 2390},
    //  {name: 'day', uv: 3490},
    //];
    let data = [];
    if (this.state.data_loads) {
      console.log(this.state.data.length)

      let mv_mday = 0, mv_mth = 0;
      let dayNO = 1;
      for (mv_mth = 0; mv_mth < this.state.data.length; mv_mth++) {
        for (mv_mday = 0; mv_mday < this.state.data[mv_mth].length; mv_mday++) {
          var obj = {
            name: dayNO.toString(),
            passenger: this.state.data[mv_mth][mv_mday][2]
          }
          data.push(obj);
          dayNO++;
        }
      }
    }
return (
      <div>
        <Panel>
          <Panel.Body>All data</Panel.Body>
        </Panel>
        <div className="charts_div">
          {this.state.data_loads ?
            <div>
              <AreaChart width={1024} height={768} data={data} margin={{ top: 10, right: 30, left: 0, bottom: 30 }} className="charts" >
                <CartesianGrid strokeDasharray="3 3" stroke="#909090" />
                <XAxis dataKey="name" stroke="#eaeaea">
                  <Label value="Days" offset={0} position="insideBottom" fill="#fff" />
                </XAxis>
                <YAxis stroke="#eaeaea" />
                <Tooltip />
                <Legend />
                <Brush />
                <Area type='monotone' dataKey='passenger' stroke='#59c3c3' fill='#59c3c3' />
              </AreaChart>
              <div className="overflow_pannel">
                <img className="pannel_show_lines" src="http://localhost:8000/static_imgs/ranking.png" alt="" />
              </div>
              <div align="center" style={{margin: '20px'}}>
              <table>
                  <tr>
                    <th>日期</th>
                    <th>事件</th>
                  </tr>
                  <tr>
                    <td width="40%">85/7/31</td>
                    <td>賀伯颱風</td>
                  </tr>
                  <tr>
                    <td>86/3/28</td>
                    <td>淡水線、北投支線通車</td>
                  </tr>
                  <tr>
                    <td>86/8/18</td>
                    <td>溫妮颱風</td>
                  </tr>
                  <tr>
                    <td>86/12/25</td>
                    <td>淡水線延伸至台北車站</td>
                  </tr>
                  <tr>
                    <td>87/10/16</td>
                    <td>瑞伯颱風</td>
                  </tr>
                  <tr>
                    <td>87/10/25</td>
                    <td>芭比絲颱風</td>
                  </tr>
                  <tr>
                    <td>87/12/24</td>
                    <td>中和線通車</td>
                  </tr>
                  <tr>
                    <td>88/9/21</td>
                    <td>集集大地震</td>
                  </tr>
                  <tr>
                    <td>88/11/11</td>
                    <td>新店線通車</td>
                  </tr>
                  <tr>
                    <td>88/12/24</td>
                    <td>板南線通車</td>
                  </tr>
                  <tr>
                    <td>89/8/31</td>
                    <td>板南線延伸至新埔、小南門線通車</td>
                  </tr>
                  <tr>
                    <td>89/12/30</td>
                    <td>南港線延伸至昆陽</td>
                  </tr>
                  <tr>
                    <td>90/7/30</td>
                    <td>桃芝颱風</td>
                  </tr>
                  <tr>
                    <td>90/9/16</td>
                    <td>納莉颱風</td>
                  </tr>
                  <tr>
                    <td>91/9/6</td>
                    <td>辛樂克</td>
                  </tr>
                  <tr>
                    <td>93/8/24</td>
                    <td>艾利颱風</td>
                  </tr>
                  <tr>
                    <td>93/9/29</td>
                    <td>小碧潭支線通車</td>
                  </tr>
                  <tr>
                    <td>94/7/18</td>
                    <td>海棠颱風</td>
                  </tr>
                  <tr>
                    <td>94/8/5</td>
                    <td>麥莎颱風</td>
                  </tr>
                  <tr>
                    <td>94/9/1</td>
                    <td>泰利颱風</td>
                  </tr>
                  <tr>
                    <td>94/10/2</td>
                    <td>龍王颱風</td>
                  </tr>
                  <tr>
                    <td>95/5/31</td>
                    <td>板南線延伸至永寧</td>
                  </tr>
                  <tr>
                    <td>96/8/18</td>
                    <td>聖帕颱風</td>
                  </tr>
                  <tr>
                    <td>97/7/28</td>
                    <td>鳳凰颱風</td>
                  </tr>
                  <tr>
                    <td>97/9/13</td>
                    <td>辛樂克颱風</td>
                  </tr>
                  <tr>
                    <td>97/9/28</td>
                    <td>薔蜜颱風</td>
                  </tr>
                  <tr>
                    <td>97/12/25</td>
                    <td>板南線延伸至南港</td>
                  </tr>
                  <tr>
                    <td>98/7/4</td>
                    <td>內湖線延伸至南港展覽館</td>
                  </tr>
                  <tr>
                    <td>98/8/7</td>
                    <td>莫拉克颱風</td>
                  </tr>
                  <tr>
                    <td>99/9/19</td>
                    <td>凡那比</td>
                  </tr>
                  <tr>
                    <td>99/11/3</td>
                    <td>蘆洲線通車</td>
                  </tr>
                 
                  <tr>
                    <td>100/2/27</td>
                    <td>南港線延伸至南港展覽館</td>
                  </tr>
                  <tr>
                    <td>101/1/5</td>
                    <td>新莊線通車</td>
                  </tr>
                  <tr>
                    <td>101/8/2</td>
                    <td>蘇拉颱風</td>
                  </tr>
                  <tr>
                    <td>101/9/30</td>
                    <td>中和新盧線全線通車</td>
                  </tr>
                  <tr>
                    <td>102/8/21</td>
                    <td>潭美颱風</td>
                  </tr>
                  <tr>
                    <td>102/6/29</td>
                    <td>新莊線延伸至迴龍</td>
                  </tr>
                  <tr>
                    <td>102/11/24</td>
                    <td>信義線通車</td>
                  </tr>
                  <tr>
                    <td>103/7/23</td>
                    <td>麥德姆颱風</td>
                  </tr>
                  <tr>
                    <td>103/12/31</td>
                    <td>跨年活動</td>
                  </tr>
                  <tr>
                    <td>103/11/15</td>
                    <td>松山線通車</td>
                  </tr>
                  <tr>
                    <td>104/7/6</td>
                    <td>土城線延伸至頂埔</td>
                  </tr>
                  <tr>
                    <td>104/7/9</td>
                    <td>昌鴻颱風</td>
                  </tr>
                  <tr>
                    <td>104/8/8</td>
                    <td>蘇迪勒颱風</td>
                  </tr>
                  <tr>
                    <td>105/7/8</td>
                    <td>松山車站爆炸案</td>
                  </tr>
                  <tr>
                    <td>105/9/17</td>
                    <td>馬勒卡颱風</td>
                  </tr>
                  <tr>
                    <td>105/9/26</td>
                    <td>梅姬颱風</td>
                  </tr>
                  <tr>
                    <td>106/7/30</td>
                    <td>海棠颱風</td>
                  </tr>
                </table>
              </div>
            </div>
            :
            <div className="pannel_loader">
              <div className="loader_main"></div>
              <p>Loading</p>
            </div>}
        </div>
      </div>
    )
  }
}

class Weeks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      data_loads: false,
      data_weeks_text: 'Monday'
    };
    this.monday_load = this.monday_load.bind(this);
    this.tuesday_load = this.tuesday_load.bind(this);
    this.wednesday_load = this.wednesday_load.bind(this);
    this.thursday_load = this.thursday_load.bind(this);
    this.friday_load = this.friday_load.bind(this);
    this.saturday_load = this.saturday_load.bind(this);
    this.sunday_laod = this.sunday_laod.bind(this);
  }

  componentDidMount() {
    fetch('http://localhost:8000/weeks/monday')//change it to WAN IP before we deploy our project
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson,
          data_loads: true
        });
      });
  }

  click_data(params) {
    fetch('http://localhost:8000/weeks/' + params)//change it to WAN IP before we deploy our project
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson,
          data_loads: true
        });
      });
  }

  monday_load() {
    this.click_data('monday');
    this.setState({
      data_weeks_text: 'Monday'
    });
  }

  tuesday_load() {
    this.click_data('tuesday');
    this.setState({
      data_weeks_text: 'Tuesday'
    });
  }

  wednesday_load() {
    this.click_data('wednesday');
    this.setState({
      data_weeks_text: 'Wednesday'
    });
  }

  thursday_load() {
    this.click_data('thursday');
    this.setState({
      data_weeks_text: 'Thursday'
    });
  }

  friday_load() {
    this.click_data('friday');
    this.setState({
      data_weeks_text: 'Friday'
    });
  }

  saturday_load() {
    this.click_data('saturday');
    this.setState({
      data_weeks_text: 'Saturday'
    });
  }

  sunday_laod() {
    this.click_data('sunday');
    this.setState({
      data_weeks_text: 'Sunday'
    });
  }
render() {
    let data_print = []
    if (this.state.data_loads) {
      console.log(this.state.data.length);
      var mv = 0;
      for (mv = 0; mv < this.state.data.length; mv++) {
        var obj = {
          name: mv.toString(),
          passenger: this.state.data[mv][2]
        };
        data_print.push(obj);
      }
      console.log(data_print);
    }
    return (
      <div>
        <Nav bsStyle="pills">
          <NavItem eventKey={1} title="Monday" onClick={this.monday_load}>
            Monday
          </NavItem>
          <NavItem eventKey={2} title="Tuesday" onClick={this.tuesday_load}>
            Tuesday
          </NavItem>
          <NavItem eventKey={3} title="Wednesday" onClick={this.wednesday_load}>
            Wednesday
          </NavItem>
          <NavItem eventKey={4} title="Thursday" onClick={this.thursday_load}>
            Thursday
          </NavItem>
          <NavItem eventKey={5} title="Friday" onClick={this.friday_load}>
            Friday
          </NavItem>
          <NavItem eventKey={6} title="Saturday" onClick={this.saturday_load}>
            Saturday
          </NavItem>
          <NavItem eventKey={7} title="Sunday" onClick={this.sunday_laod}>
            Sunday
          </NavItem>
        </Nav>
        <Panel>
          <Panel.Body>{this.state.data_weeks_text}</Panel.Body>
        </Panel>
        <div className="charts_div">
          {this.state.data_loads ?
            <AreaChart width={1024} height={768} data={data_print} margin={{ top: 10, right: 30, left: 0, bottom: 0 }} className="charts">
              <CartesianGrid strokeDasharray="3 3" stroke="#909090" />
              <XAxis dataKey="name" stroke="#eaeaea">
                <Label value="Days" offset={0} position="insideBottom" fill="#fff" />
              </XAxis>
              <YAxis stroke="#eaeaea" />
              <Tooltip />
              <Legend />
              <Area type='monotone' dataKey='passenger' stroke='#f46197' fill='#f46197' />
            </AreaChart>
            :
            <div className="pannel_loader">
              <div className="loader_weeks"></div>
              <p>Loading</p>
            </div>}
        </div>
      </div>
    )
  }
}

class Predict extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      data_loads: false,
      show_texts: false,
      URL01: '',
      URL02: '',
      URL03: '',
      URL04: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ value: event.target.value });
  }
handleSubmit(event) {
    if (this.state.value.toString() > 0) {
      this.setState({
        data_loads: true
      });
      fetch('http://localhost:8000/predict?predict_days=' + this.state.value)//change it to WAN IP before we deploy our project
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            URL01: "http://localhost:8000/images/" + responseJson.URL01 + ".png",
            URL02: "http://localhost:8000/images/" + responseJson.URL02 + ".png",
            URL03: "http://localhost:8000/images/" + responseJson.URL03 + ".png",
            URL04: "http://localhost:8000/images/" + responseJson.URL04 + ".png",
            show_texts: true,
            data_loads: false
          });
          console.log(responseJson.URL);
        });
    } else {
      alert("You must input number >0 !");
    }
    event.preventDefault();
  }
  render() {
    return (
      <div>
        <form>
          <FormGroup>
            <Grid>
              <ControlLabel className="ctl_label">Please input the days after 2018/11/30 which you'd like to predict in the future.</ControlLabel>
              <Row className="show-grid">
                <Col xs={6} md={4}>
                  <FormControl type="text" value={this.state.value} placeholder="Enter the value" onChange={this.handleChange} />
                </Col>
                <Col>
                  <Button type="submit" onClick={this.handleSubmit}>Predict</Button>
                </Col>
              </Row>
            </Grid>
          </FormGroup>
        </form>
        {
          this.state.data_loads ?
            <div className="pannel_loader">
              <div className="loader_predict"></div>
              <p className="white_text">Predicting...</p>
            </div>
            :
            <div>
              {this.state.show_texts? <p className="white_text center_text">Naive Forecast Method</p>: null}
              <div className="overflow_pannel">
                <img className="pannel_show_lines" src={this.state.URL04} alt="" />
              </div>
              {this.state.show_texts? <p className="white_text center_text">Traditional Predict Method</p>: null}
              <div className="overflow_pannel">
                <img className="pannel_show_lines" src={this.state.URL01} alt="" />
              </div>
              {this.state.show_texts? <p className="white_text center_text">Linear Regression Method</p>: null}
              <div className="overflow_pannel">
                <img className="pannel_show_lines" src={this.state.URL02} alt="" />
              </div>
              {this.state.show_texts? <p className="white_text center_text">Linear Regression(2) Method</p>: null}
              <div className="overflow_pannel">
                <img className="pannel_show_lines" src={this.state.URL03} alt="" />
              </div>
            </div>
        }
      </div>
    )
  }
}

class Gallery extends Component {
  render() {
    return (
      <div>
        <p className="white_text center_text">Spot Plot</p>
        <div className="overflow_pannel">
          <img className="pannel_show_lines" src="http://localhost:8000/static_imgs/boxplot.png" alt="" />
        </div>
        <p className="white_text center_text">Box Plot</p>
        <div className="overflow_pannel">
          <img className="pannel_show_lines" src="http://localhost:8000/static_imgs/spot_fig.png" alt="" />
        </div>
        <p className="white_text center_text">Line Plot</p>
        <div className="overflow_pannel">
          <img className="pannel_show_lines" src="http://localhost:8000/static_imgs/line_fig.png" alt="" />
        </div>
      </div>
    )
  }
}

export default App;