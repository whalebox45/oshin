import json
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

data_monday = []
data_tuesday = []
data_wednesday = []
data_thursday = []
data_friday = []
data_saturday = []
data_sunday = []

for mv_x in range(8504, 10712, 1):
     if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        continue
     if (mv_x % 100) == 0:
        continue
     
     with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)
 
     for x in range(len(data)):
        if data[x][1] == '一':
            data_monday.append(int(data[x][2]))
        elif data[x][1] == '二':
            data_tuesday.append(int(data[x][2]))
        elif data[x][1] == '三':
            data_wednesday.append(int(data[x][2]))
        elif data[x][1] == '四':
            data_thursday.append(int(data[x][2]))
        elif data[x][1] == '五':
            data_friday.append(int(data[x][2]))
        elif data[x][1] == '六':
            data_saturday.append(int(data[x][2]))
        elif data[x][1] == '日':
            data_sunday.append(int(data[x][2]))
        else:
            pass

plt.plot(data_monday, 'ro', color='red')
plt.plot(data_tuesday, 'ro', color='orange')
plt.plot(data_wednesday, 'ro', color='yellow')
plt.plot(data_thursday, 'ro', color='green')
plt.plot(data_friday, 'ro', color='blue')
plt.plot(data_saturday, 'ro', color='purple')
plt.plot(data_sunday, 'ro', color='grey')
plt.figure()

plt.boxplot([data_monday, data_tuesday, data_wednesday, data_thursday, data_friday, data_saturday, data_sunday], 1, '', showmeans=True)
plt.show()

# data_monday = np.asarray(data_monday)
# data_tuesday = np.asarray(data_tuesday)
# data_wednesday = np.asarray(data_wednesday)
# data_thursday = np.asarray(data_thursday)
# data_friday = np.asarray(data_friday)
# data_saturday = np.asarray(data_saturday)
# data_sunday = np.asarray(data_sunday)
# 
# print(np.mean(data_monday))
# print(np.mean(data_tuesday))
# print(np.mean(data_wednesday))
# print(np.mean(data_thursday))
# print(np.mean(data_friday))
# print(np.mean(data_saturday))
# print(np.mean(data_sunday))

# plt.plot(np.mean(data_monday), 'k-', color='red')
# plt.plot(np.mean(data_tuesday), 'k-', color='orange')
# plt.plot(np.mean(data_wednesday), 'k-', color='yellow')
# plt.plot(np.mean(data_thursday), 'k-', color='green')
# plt.plot(np.mean(data_friday), 'k-', color='blue')
# plt.plot(np.mean(data_saturday), 'k-', color='purple')
# plt.plot(np.mean(data_sunday), 'k-', color='grey')

# plt.plot([0, 1183], [np.mean(data_monday), np.mean(data_monday)], 'k-', color='red')
# plt.plot([0, 1183], [np.mean(data_tuesday), np.mean(data_tuesday)], 'k-', color='orange')
# plt.plot([0, 1183], [np.mean(data_wednesday), np.mean(data_wednesday)], 'k-', color='yellow')
# plt.plot([0, 1183], [np.mean(data_thursday), np.mean(data_thursday)], 'k-', color='green')
# plt.plot([0, 1183], [np.mean(data_friday), np.mean(data_friday)], 'k-', color='blue')
# plt.plot([0, 1183], [np.mean(data_saturday), np.mean(data_saturday)], 'k-', color='purple')
# plt.plot([0, 1183], [np.mean(data_sunday), np.mean(data_sunday)], 'k-', color='grey')
