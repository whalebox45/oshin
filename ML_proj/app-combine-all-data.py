# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
import json
import numpy as np

data_num = []
for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        #mv_x = mv_x - 12 + 100
        continue
    if (mv_x % 100) == 0:
        #mv_x = mv_x - 12 + 100
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding='utf-8') as f:
        data = json.load(f)
        data_num.append(data)

with open('all-data.json', 'w', encoding='utf-8') as outfile:
    outfile.write(json.dumps(data_num, ensure_ascii=False))

#print(data_num)