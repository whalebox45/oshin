# coding=UTF-8

import json
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

data_num = []

data_date = []

for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        continue
    if (mv_x % 100) == 0:
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)

    for x in range(len(data)):
        data_num.append(int(data[x][2]))
        data_date.append(data[x][0])

data_rows = []
for x in range(len(data_num)):
    data_rows.append(x)

# print(data_rows)
# print(data_num)

for mv in range(len(data_num)):
    print("Date: {} -- Passenger: {}".format(data_date[mv], data_num[mv]))

data_rows = np.asarray(data_rows) #x
data_num = np.asarray(data_num) #y

gradient, intercept, r_value, p_value, std_err = stats.linregress(data_rows, data_num)

res_y = gradient * data_rows + intercept

# model = LinearRegression()

# data_rows = data_rows.reshape(data_rows.size, 1)
# data_num = data_num.reshape(data_num.size, 1)
# 
# model.fit(data_rows, data_num)
# 
# x_test = np.array([8279, 8280, 8281, 8282], dtype='int32')
# x_test = np.zeros((3650), dtype="int32")
# for x in range(8279, 3650 + 8279, 1):
#     x_test[x - 8279] = x
# 
# predictions = model.predict(x_test.reshape(x_test.size, 1))
# 
# print(predictions)

plt.plot(data_rows, data_num, color='green')
plt.plot(data_rows, res_y, 'blue')
# plt.plot(x_test, predictions.flatten(), 'red') # predict line

plt.xlabel('Date')
plt.ylabel('Population')
plt.title('Line chart')
plt.show()

deviation = []

for x in range(data_num.size - 1):
    deviation.append([data_date[x], int(abs(data_num[x] - data_num[x + 1]))])

for x in range(data_num.size - 1):
    print("Date: {} <--> {} -- Difference: {}".format(data_date[x], data_date[x + 1], deviation[x][1]))

print(deviation)

print("Max difference: {}".format(max(deviation, key = lambda x: x[1])))
print("Min difference: {}".format(min(deviation, key = lambda x: x[1])))

deviation = sorted(deviation, key=lambda s: s[1], reverse=True)

f = open("Difference_max_min.json", "w")
f.write(json.dumps(deviation, ensure_ascii=False))
f.close()

show_first_20 = []

for x in deviation:
    show_first_20.append(x[1])
    
print(show_first_20)

plt.bar(np.arange(100), show_first_20[0: 100])
plt.show()
# plt.bar(diff_days, deviation)
# plt.show()

