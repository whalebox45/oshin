import json
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

data_num = []
for mv_x in range(8504, 10712, 1):
    if (mv_x % 100) > 12 and (mv_x % 100) <= 99:
        #mv_x = mv_x - 12 + 100
        continue
    if (mv_x % 100) == 0:
        #mv_x = mv_x - 12 + 100
        continue
    
    with open('./passenger_data_json/{}.json'.format(mv_x), encoding = 'utf8') as f:
        data = json.load(f)

    #data_rows = np.arange(len(data), dtype='int32')
    #print(data_rows)

    #data_num = np.zeros(len(data), dtype='int32')
    #for x in range(len(data)):
    #    data_num[x] = int(data[x][2])

    #print(data_num)

    for x in range(len(data)):
        data_num.append(int(data[x][2]))

data_rows = []
for x in range(len(data_num)):
    data_rows.append(x)

print(len(data_rows))
print(data_rows)
print(len(data_num))
print(data_num)

data_rows = np.asarray(data_rows) #x
data_num = np.asarray(data_num) #y

gradient, intercept, r_value, p_value, std_err = stats.linregress(data_rows, data_num)

res_y = gradient * data_rows + intercept

model = LinearRegression()

data_rows = data_rows.reshape(data_rows.size, 1)
data_num = data_num.reshape(data_num.size, 1)

model.fit(data_rows, data_num)

x_test = np.array([8279, 8280, 8281, 8282], dtype='int32')
x_test = np.zeros((3650), dtype="int32")
for x in range(8279, 3650 + 8279, 1):
    x_test[x - 8279] = x

predictions = model.predict(x_test.reshape(x_test.size, 1))

print(predictions)

plt.plot(data_rows, data_num, color='green')
plt.plot(data_rows, res_y, 'blue')
plt.plot(x_test, predictions.flatten(), 'red') # predict line

#plt.xlim((0, len(data_num) - 1))
plt.xlabel('Date')
plt.ylabel('Population')
plt.title('Line chart')
#plt.tight_layout()
plt.show()