# -*- coding: utf-8 -*-

import json
import matplotlib.pyplot as plt

with open('Difference_max_min.json') as data_file:  
    data = json.load(data_file)

data_num = []
for x in data:
    data_num.append(x[1])

average = sum(data_num) / len(data_num)

data_10 = data[: 10]
show_num = []
show_date = []

for x in data_10:
    show_num.append(x[1])
    show_date.append(x[0])

show_num.append(average)
show_date.append("Average")

barlist = plt.bar(range(len(show_num)), show_num, color='#1f77b4')
plt.xticks(range(len(show_num)), show_date, rotation=45)
barlist[0].set_color('red')
barlist[10].set_color('green')
plt.xlabel('Date')
plt.ylabel('Passenger')
plt.title('Maxinum Difference Between Two Days')
plt.show()
